package com.example.excel.controller;

import com.example.excel.com.Account;
import com.example.excel.service.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Base64;
import java.util.Date;

@Service
public class AccountService {

    private AccountRepository accountRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }
    public String createAccountAndGenerateToken(Account account) {
        Account newAccount = accountRepository.save(account);
        String encodedSecretKey = Base64.getEncoder().encodeToString("nD8f2Xbr6sLz9YtQwewEHGh3Kp5KmNqSewEHGh3Kp58f2X".getBytes());

        String jwtToken = Jwts.builder()
                .claim("username", newAccount.getUsername())
                .claim("password",newAccount.getPassword())
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, encodedSecretKey)
                .compact();


        return jwtToken;
    }

}
