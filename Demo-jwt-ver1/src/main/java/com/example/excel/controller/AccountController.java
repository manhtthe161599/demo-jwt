package com.example.excel.controller;

import com.example.excel.com.Account;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private JwtUtil jwtUtil;

    @PostMapping("/accounts")
    public String createAccountAndGenerateToken(@RequestBody Account account) {
        return accountService.createAccountAndGenerateToken(account);
    }


    @GetMapping("/accounts/{token}")
    public Claims decodeToken(@PathVariable String token) {
        Claims claims = jwtUtil.decodeJWT(token);
        return claims;
    }



}



